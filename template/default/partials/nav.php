<div class="s-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul class="s-navbar">
                  <li><a href="index.php?keywords=&search=search" <?php echo (isset($_GET['search']) && $_GET['search'] == 'search') ? 'class="active"' : '' ?>><?php echo __('Collections'); ?></a></li>
                  <li><a href="index.php?p=news" <?php echo (isset($_GET['p']) && $_GET['p'] == 'news') ? 'class="active"' : '' ?>><?php echo __('News'); ?></a></li>
                  <li><a href="index.php?p=libinfo" <?php echo (isset($_GET['p']) && $_GET['p'] == 'libinfo') ? 'class="active"' : '' ?>><?php echo __('Library Information'); ?></a></li>
                  <li><a href="index.php?p=member" <?php echo (isset($_GET['p']) && $_GET['p'] == 'member') ? 'class="active"' : '' ?>><?php echo __('Member Area'); ?></a></li>
                  <li><a href="index.php?p=librarian" <?php echo (isset($_GET['p']) && $_GET['p'] == 'librarian') ? 'class="active"' : '' ?>><?php echo __('Librarian'); ?></a></li>
                  <li><a href="index.php?p=peta" class="openPopUp" width="600" height="400"><?php echo __('Library Location'); ?></a></li>
                </ul>
            </div>
            <div class="col-md-5">
                <form action="index.php" method="get" autocomplete="off" class="s-search form-inline">
                    <input type="text" class="form-control" name="keywords" lang="<?php echo $sysconf['default_lang']; ?>" role="search" placeholder="<?php echo __('Search Collection') ?> ...">
                    <button class="btn btn-success" name="search" value="search" type="submit">Go</button>
                    <a href="#advance-search" class="btn btn-info inline"><?php echo __('Advanced Search') ?></a>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
  $(".inline").colorbox({inline:true, html:"<h1>Welcome</h1>", width:"50%"});
</script>
