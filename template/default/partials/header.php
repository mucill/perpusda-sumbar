<header>
    <div class="s-call">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="index.php"> <?php echo __('Home') ?></a>
                    <a href="index.php?p=login"> <?php echo __('Login') ?></a>
                    <a href="index.php?p=visitor"> <?php echo __('Visitor') ?></a>
                    <a href="#">Facebook</a>
                    <a href="#">Twitter</a>
                </div>
                <div class="col-md-6 text-right">
                    <a href="tel: 07517051348"></span> 0751-7051348</a>
                    <a href="mailto: hallo@pupukkaltim.com">info@ebook.pustaka.sumbarprov.go.id</a>
                </div>
                <div class="col-md-2 text-right">
                  <form class="s-lang form" name="langSelect" action="index.php" method="get">
                    <select name="select_lang" id="select_lang" title="Change language of this site" onchange="document.langSelect.submit();" class="form-control">
                      <?php echo $language_select; ?>
                    </select>
                  </form>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 s-brand">
                <a href="index.php">
                <h1><?php echo $sysconf['library_name']?></h1>
                <p><?php echo $sysconf['library_subname']?></p>
                </a>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3 text-right">
            </div>
        </div>
    </div>
</header>
