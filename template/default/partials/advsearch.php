<div style="display: none;">
<div id="advance-search" class="container">
<form action="index.php" method="get">
<table width="100%" border="0" cellspacing="10">
  <tr>
    <td width="5%"><?php echo __('Title'); ?></td>
    <td width="40%"><input type="text" name="title" /></td>
    <td width="5%">&nbsp;</td>
    <td width="5%"><?php echo __('Collection Type'); ?></td>
    <td width="40%"><select name="colltype"><?php echo $colltype_list; ?></select></td>
  </tr>
  <tr>
    <td><?php echo __('Author(s)'); ?></td>
    <td><input type="text" name="author" /></td>
    <td width="5%">&nbsp;</td>
    <td><?php echo __('Location'); ?></td>
    <td><select name="location"> <?php echo $location_list; ?></select></td>
  </tr>
  <tr>
    <td><?php echo __('Subject(s)'); ?></td>
    <td><input type="text" name="subject" /></td>
    <td width="5%">&nbsp;</td>
    <td><?php echo __('GMD'); ?></td>
    <td><select name="gmd"><?php echo $gmd_list; ?></select></td>
  </tr>
  <tr>
    <td><?php echo __('Isbn / Issn'); ?></td>
    <td><input type="text" name="isbn" /></td>
    <td width="5%">&nbsp;</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
      <input type="hidden" name="searchtype" value="advance" />
      <button type="submit" name="search" value="search"><?php echo __('Search'); ?></button>
    </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
</form>
</div>
</div>
