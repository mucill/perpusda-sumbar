<?php
/**
 * Template for OPAC
 *
 * Copyright (C) 2015 Arie Nugraha (dicarve@gmail.com) Eddy Subratha (eddy.subratha@slims.web.id)
 *
 * Slims 8 (Akasia)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

// be sure that this file not accessed directly

if (!defined('INDEX_AUTH')) {
  die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
  die("can not access this file directly");
}

?>
<!--
==========================================================================
   ___  __    ____  __  __  ___      __    _  _    __    ___  ____    __
  / __)(  )  (_  _)(  \/  )/ __)    /__\  ( )/ )  /__\  / __)(_  _)  /__\
  \__ \ )(__  _)(_  )    ( \__ \   /(__)\  )  (  /(__)\ \__ \ _)(_  /(__)\
  (___/(____)(____)(_/\/\_)(___/  (__)(__)(_)\_)(__)(__)(___/(____)(__)(__)

==========================================================================
-->
<!DOCTYPE html>
<html lang="<?php echo substr($sysconf['default_lang'], 0, 2); ?>" xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns#">
<head>

<?php
// Meta Template
include "partials/meta.php";
?>

</head>

<body itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div class="wrapper">
<!--[if lt IE 9]>
<div class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</div>
<![endif]-->

<?php
// Header
include "partials/header.php";
?>

<?php
// Navigation
include "partials/nav.php";
?>

<?php if(isset($_GET['search']) || isset($_GET['p'])): ?>
<div class="s-content">
    <div class="container">
        <div class="row">
            <main class="col-md-8">
              <div class="s-page">
                <?php
                if(strlen($main_content) == 7) {
                  echo '<h2>' . __('No Result') . '</h2><hr/><p>' . __('Please try again') . '</p>';
                } else {
                  if(isset($_GET['search'])) echo '<div class="row">';
                  echo $main_content;
                  if(isset($_GET['search'])) echo '</div>';
                }

                // hack the layout
                if(isset($_GET['search']) || (isset($_GET['p']) && $_GET['p'] != 'member')){
                  echo '</div>';
                } else {
                  if(isset($_SESSION['mid'])) {
                    echo  '</div></div>';
                  }
                }
                ?>
            </main>
            <div class="col-md-4">
                <div class="s-sidebar">

                  <?php if(!isset($_GET['p']) && isset($_GET['keywords']) && ($_GET['keywords'] != '')) : ?>
                  <div class="card">
                      <p class="card-header"><?php echo __('Search Result'); ?></p>
                      <div class="card-block">
                        <?php echo $search_result_info ?>
                      </div>
                  </div>
                  <?php endif ?>

                  <div class="card">
                      <p class="card-header"><?php echo __('Information'); ?></p>
                      <div class="card-block">
                        <?php echo (utility::isMemberLogin()) ? $header_info : $info; ?>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php else: ?>

<div class="s-slider">
    <img src="template/default/img/slider.jpg" alt="">
</div>

<div class="s-feature">
    <div class="container">
        <div class="row">
        <?php
        // Promoted titles - Only show at the homepage
        if(  !( isset($_GET['search']) || isset($_GET['title']) || isset($_GET['keywords']) || isset($_GET['p']) ) ) :
          // query top book
          $topbook = $dbs->query('SELECT a.biblio_id, a.title, a.image, b.gmd_name FROM biblio a LEFT JOIN mst_gmd b ON a.gmd_id = b.gmd_id WHERE a.promoted = 1 ORDER BY a.last_update DESC LIMIT 6');
          if ($num_rows = $topbook->num_rows) :
            while ($book = $topbook->fetch_assoc()) :
              $title = explode(" ", $book['title']);
              if (!empty($book['image'])) : ?>
              <div class="col-md-2">
                <a itemprop="name" property="name" href="./index.php?p=show_detail&amp;id=<?php echo $book['biblio_id'] ?>" title="<?php echo $book['title'] ?>">
                  <div class="s-book-cover">
                    <img itemprop="image" src="images/docs/<?php echo $book['image'] ?>" alt="<?php echo $book['title'] ?>" >
                  </div>
                  <div class="s-book-title">
                      <div><?php echo $book['gmd_name'] ?></div>
                      <?php echo $book['title'] ?>
                  </div>
                </a>
              </div>
          <?php endif; /* end if $book[image] */ endwhile /*end while $topbook */; endif /* end numrow */; endif; /* end if set */ ?>
        </div>
    </div>
</div>

<?php endif; /* end if p is set */ ?>
<?php
// Advance Search
include "partials/advsearch.php";

// Footer
include "partials/footer.php";

// Chat Engine
include LIB."contents/chat.php";

?>
</div>
<script>
  $('.biblioMarkForm').addClass('row');
</script>
</body>
</html>
